﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using LibplctagWrapper;

namespace AllenBradleyLinkTest
{
    public partial class Form1 : Form
    {
        const int DataTimeout = 5000;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Libplctag client = new Libplctag();
            // create the tag (We want to read TestDINTArray[0], TestDINTArray[1], 
            // TestDINTArray[2],TestDINTArray[3])
            var tag = new Tag("192.168.0.39", "1, 0", 
                              CpuType.LGX, "TestDINTArray[0]",
                              DataType.Int32, 1);

            // add the tag
            client.AddTag(tag);

            // check that the tag has been added, if it returns pending we have to retry
            while (client.GetStatus(tag) == Libplctag.PLCTAG_STATUS_PENDING)
            {
                Thread.Sleep(100);
            }

            // if the status is not ok, we have to handle the error
            if (client.GetStatus(tag) != Libplctag.PLCTAG_STATUS_OK)
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.Text = ($"Error setting up tag internal state. Error{client.DecodeError(client.GetStatus(tag))}\n");
                Console.WriteLine($"Error setting up tag internal state. Error{ client.DecodeError(client.GetStatus(tag))}\n");
                return;
            }

            // Execute the read
            var result = client.ReadTag(tag, DataTimeout);

            // Check the read operation result
            if (result != Libplctag.PLCTAG_STATUS_OK)
            {
                lblStatus.BackColor = Color.Red;
                lblStatus.Text = ($"ERROR: Unable to read the data! Got error code {result}: {client.DecodeError(result)}\n");
                Console.WriteLine($"ERROR: Unable to read the data! Got error code {result}: {client.DecodeError(result)}\n");
                return;
            }
            // Convert the data
            var TestDintArray0 = client.GetInt32Value(tag, 0 * tag.ElementSize); // multiply with tag.ElementSize to keep indexes consistant with the indexes on the plc
            //var TestDintArray1 = client.GetInt32Value(tag, 1 * tag.ElementSize);
            //var TestDintArray2 = client.GeytInt32Value(tag, 2 * tag.ElementSize);
            //var TestDintArray3 = client.GetInt32Value(tag, 3 * tag.ElementSize);

            // print to console
            Console.WriteLine("TestDintArray0: " + TestDintArray0);
            //Console.WriteLine("TestDintArray1: " + TestDintArray1);
            //Console.WriteLine("TestDintArray2: " + TestDintArray2);
            //Console.WriteLine("TestDintArray3: " + TestDintArray3);

            lblValue.Text = TestDintArray0.ToString("0000");
            lblStatus.BackColor = Color.DarkGreen;
            lblStatus.Text = ($"LECTURA EXITOSA");
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
